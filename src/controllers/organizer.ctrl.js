const model = require('../models/organizer.model');
const controller = {};

controller.getAll = async (req, res) => {
    const list = await model.find();
    res.json({ status: 'success', body: list });
};

controller.getOne = async (req, res) => {
    const { id } = req.params
    const orgzr = await model.findById(id);
    res.json({ status: 'success', body: orgzr });
};

controller.setOne = async (req, res) => {

    const {
        c_name,
        pc_name,
        pc_email,
        pc_phone,
        pc_pwd,
        pc_web,
        fb_page,
        ig_page,
        adt_cts
    } = req.body;

    const newOrg = new model({
        c_name,
        pc_name,
        pc_email,
        pc_phone,
        pc_pwd,
        pc_web,
        fb_page,
        ig_page,
        adt_cts
    });

    await newOrg.save();
    res.json({ status: 'success', body: 'created' });
};

controller.update = async (req, res) => {
    const { id } = req.params;
    const {
        c_name,
        pc_name,
        pc_email,
        pc_phone,
        pc_pwd,
        pc_web,
        fb_page,
        ig_page,
        adt_cts
    } = req.body;

    const updated = await model.findByIdAndUpdate(id, {
        c_name,
        pc_name,
        pc_email,
        pc_phone,
        pc_pwd,
        pc_web,
        fb_page,
        ig_page,
        adt_cts
    });
    res.json({ status: 'success', body: updated });
};

controller.delete = async (req, res) => {
    const { id } = req.params;
    const deleted = await model.findByIdAndDelete(id);
    res.json({ status: 'success', body: deleted });
};

module.exports = controller;