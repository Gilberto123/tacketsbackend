const { model, Schema } = require('mongoose');
const category = require('./generic/category.model');
const cupon = require('./generic/cupon.model');

const schema = new Schema({
    organizer: 'string', // Organizer id
    type: 'string', // Public or Private
    cls: 'string', // MTB ROUTE TRIATHLON
    name: 'string', // Event name
    start: 'string', // Date and time start
    end: 'string', // Date and time end
    main_img: 'string', // Main Img
    sponsors: 'string', // Sponsors
    location: 'string', // Venue City State Country
    fb_page: 'string', // Facebook page
    ig_page: 'string', // Instagram page
    web: 'string', // Website
    ex_ev_link: 'string', // External event ticket link
    results: 'string', // Url or Pdf
    url_maps: 'string', // Url to Google maps
    categories: [category], // Categories
    featured: Boolean, // Featured event
    cupons: [cupon]
}, {
    timestamps: true,
    versionKey: false
});

const event = model('Event', schema);

module.exports = event;