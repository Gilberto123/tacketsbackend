const { Schema } = require('mongoose');

const schema = new Schema({
    name: 'string',
    email: 'string',
    phone: 'string',
    pass: 'string'
}, {
    timestamps: true,
    versionKey: false
});

module.exports = schema;