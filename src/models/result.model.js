const { Schema, model} = require('mongoose');

const schema = new Schema({
    event: 'string',
    cat: 'string',
    part: 'string',
    time: 'string',
    position: 'string'
}, {
    timestamps: true,
    versionKey: false
});

const result = model('Result', schema);

module.exports = result;