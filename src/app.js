const express = require('express');
const morgan = require('morgan');
const app = express();

// settings
app.set('port', process.env.PORT || 3000);

// middlewares
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// routes
app.use('/api/organizer', require('./routes/organizer.routes'));
app.use('/api/event', require('./routes/event.routes'));
app.use('/api/result', require('./routes/result.routes'));
app.use('/api/part', require('./routes/part.routes'));
app.use('/api/question', require('./routes/question.routes'));

module.exports = app;