const { Schema, model } = require('mongoose');

const schema = new Schema({
    event: 'string',
    question: 'string',
    type: 'string',
    ans: 'string',
    opts: ['string'],
    order: Number,
    required: Boolean
}, {
    timestamps: true,
    versionKey: false
});

const q = model('Questions', schema);

module.exports = q;