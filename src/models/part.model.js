const { Schema, model } = require('mongoose');

const schema = new Schema({
    name: 'string',
    f_lst_name: 'string',
    m_lst_name: 'string',
    b_day: 'string',
    b_month: 'string',
    b_year: 'string',
    bld_type: 'string',
    alleg: 'string',
    gender: 'string',
    team: 'string',
    phone: 'string',
    email: 'string',
    conf_email: 'string',
    pass: 'string',
    address: 'string',
    emg_contact_name: 'string',
    emg_contact_rel: 'string',
    emg_contact_phone: 'string',
    pic: 'string',
    table: 'string' //TODO Table with the list of the events in which the participant was registrred to  (check with API of Athlinks or Chronotrack)
}, {
    timestamps: true,
    versionKey: false
});

const part = model('Participant', schema);

module.exports = part;