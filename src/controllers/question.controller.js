const model = require('../models/question.model');
const controller = {};

controller.get = async (req, res) => {
    const list = await model.find();
    res.json({
        message: 'success',
        body: list
    });
};

controller.getOne = async (req, res) => {
    const { id } = req.params;
    const q = await model.findById(id);
    res.json({
        message: 'success',
        body: q
    });
};

controller.setOne = async (req, res) => {
    const {
        event,
        question,
        type,
        ans,
        opts,
        order,
        required
    } = req.body;

    const newQ = new model({
        event,
        question,
        type,
        ans,
        opts,
        order,
        required
    });

    await newQ.save();

    res.json({
        message: 'success',
        body: 'created'
    });
};

controller.update = async (req, res) => {
    const { id } = req.params;
    const {
        event,
        question,
        type,
        ans,
        opts,
        order,
        required
    } = req.body;

    const updt = await model.findByIdAndUpdate(id, {
        event,
        question,
        type,
        ans,
        opts,
        order,
        required
    });

    res.json({
        message: 'success',
        body: updt
    });
};

controller.delete = async (req, res) => {
    const { id } = req.params;
    const deleted = await model.findByIdAndDelete(id);
    res.json({
        message: 'success',
        body: deleted
    });
};

module.exports = controller;