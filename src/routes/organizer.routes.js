const { Router } = require('express');
const Ctrl = require('../controllers/organizer.ctrl')
const router = Router();

router.get('/', Ctrl.getAll);

router.get('/getOne/:id', Ctrl.getOne);

router.post('/', Ctrl.setOne);

router.put('/update/:id', Ctrl.update);

router.delete('/delete/:id',Ctrl.delete);

module.exports = router;