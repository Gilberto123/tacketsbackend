const { Router } = require('express');
const router = Router();
const Ctrl = require('../controllers/part.controller');

router.get('/', Ctrl.get);

router.get('/getOne/:id', Ctrl.getOne);

router.post('/', Ctrl.setOne);

router.put('/update/:id', Ctrl.update);

router.delete('/delete/:id', Ctrl.delete)

module.exports = router;