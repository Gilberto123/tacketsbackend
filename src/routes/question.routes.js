const { Router } = require('express');
const router = Router();
const ctrl = require('../controllers/question.controller');

router.get('/', ctrl.get);

router.get('/getOne/:id', ctrl.getOne);

router.post('/', ctrl.setOne);

router.put('/update/:id', ctrl.update);

router.delete('/delete/:id', ctrl.delete)

module.exports = router;