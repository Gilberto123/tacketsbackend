const controller = {};
const model = require('../models/event.model');

controller.getAll = async (req, res) => {
    const events = await model.find();
    res.json({ status: 'success', body: events })
};

controller.getOne = async (req, res) => {
    const { id } = req.params;
    const event = await model.findById(id);
    res.json({ status: 'success', body: event });
};

controller.setOne = async (req, res) => {
    const {
        organizer,
        type,
        cls,
        name,
        start,
        end,
        main_img,
        sponsors,
        location,
        fb_page,
        ig_page,
        web,
        ex_ev_link,
        results,
        url_maps,
        categories,
        featured,
        cupons
    } = req.body;

    const newEvent = new model({
        organizer,
        type,
        cls,
        name,
        start,
        end,
        main_img,
        sponsors,
        location,
        fb_page,
        ig_page,
        web,
        ex_ev_link,
        results,
        url_maps,
        categories,
        featured,
        cupons
    });

    await newEvent.save();
    res.json({ status: 'success', body: 'created' })
};

controller.update = async (req, res) => {
    const { id } = req.params;
    const {
        organizer,
        type,
        cls,
        name,
        start,
        end,
        main_img,
        sponsors,
        location,
        fb_page,
        ig_page,
        web,
        ex_ev_link,
        results,
        url_maps,
        categories,
        featured,
        cupons
    } = req.body;

    const updated = await model.findByIdAndUpdate(id, {
        organizer,
        type,
        cls,
        name,
        start,
        end,
        main_img,
        sponsors,
        location,
        fb_page,
        ig_page,
        web,
        ex_ev_link,
        results,
        url_maps,
        categories,
        featured,
        cupons
    });
    res.json({ status: 'success', body: updated });
};

controller.delete = async (req, res) => {
    const { id } = req.params;
    const deleted = await model.findByIdAndDelete(id);
    res.json({ status: 'success', body: deleted });
};

module.exports = controller;