const mongoose = require('mongoose');

const main = async () => {
    await mongoose.connect('mongodb://localhost:27017/tickets');
    console.log('DB is connected');
}

main();