const { Router } = require('express');
const router = Router();
const Ctrl = require('../controllers/result.controller');

router.get('/about/:part', Ctrl.getPartRes);

router.get('/:ev', Ctrl.getEvRes);

router.get('/:ev/:cat', Ctrl.getEvCatRes);

router.get('/:ev/:cat/:part', Ctrl.getEvCatPartRes);

router.post('/', Ctrl.setOne);

router.put('/update/:id', Ctrl.update);

router.delete('/delete/:id', Ctrl.delete);

module.exports = router;