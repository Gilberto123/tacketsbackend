const model = require('../models/part.model');
const controller = {};

controller.get = async (req, res) => {
    const parts = await model.find();
    res.json({
        message: 'success',
        body: parts
    });
};

controller.getOne = async (req, res) => {
    const { id } = req.params;
    const part = await model.findById(id);
    res.json({
        massage: 'success',
        body: part
    });
};

controller.setOne = async (req, res) => {
    const {
        name,
        f_lst_name,
        m_lst_name,
        b_day,
        b_month,
        b_year,
        bld_type,
        alleg,
        gender,
        team,
        phone,
        email,
        conf_email,
        pass,
        address,
        emg_contact_name,
        emg_contact_rel,
        emg_contact_phone,
        pic,
        table
    } = req.body;

    const newPart = new model({
        name,
        f_lst_name,
        m_lst_name,
        b_day,
        b_month,
        b_year,
        bld_type,
        alleg,
        gender,
        team,
        phone,
        email,
        conf_email,
        pass,
        address,
        emg_contact_name,
        emg_contact_rel,
        emg_contact_phone,
        pic,
        table
    });

    await newPart.save();
    res.json({
        message: 'success',
        body: 'Created'
    });
};

controller.update = async (req, res) => {
    const { id } = req.params;
    const {
        name,
        f_lst_name,
        m_lst_name,
        b_day,
        b_month,
        b_year,
        bld_type,
        alleg,
        gender,
        team,
        phone,
        email,
        conf_email,
        pass,
        address,
        emg_contact_name,
        emg_contact_rel,
        emg_contact_phone,
        pic,
        table
    } = req.body;

    const updt = await model.findByIdAndUpdate(id, {
        name,
        f_lst_name,
        m_lst_name,
        b_day,
        b_month,
        b_year,
        bld_type,
        alleg,
        gender,
        team,
        phone,
        email,
        conf_email,
        pass,
        address,
        emg_contact_name,
        emg_contact_rel,
        emg_contact_phone,
        pic,
        table
    });

    res.json({
        message: 'success',
        body: updt
    });
};

controller.delete = async (req, res) => {
    const { id } = req.params;
    const deleted = await model.findByIdAndDelete(id);
    res.json({
        message: 'success',
        body: deleted
    });
}

module.exports = controller;