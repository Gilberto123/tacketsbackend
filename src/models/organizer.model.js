const { model, Schema } = require('mongoose');
const adtContact = require('./generic/adtContact.model');

const schema = new Schema({
    c_name: 'string', // Company name
    pc_name: 'string', // Primary contact name
    pc_email: 'string', // Primary contact email
    pc_phone: 'string', // Primary contact phone
    pc_pwd: 'string', // Primary contact password
    pc_web: 'string', // Primary contact website
    fb_page: 'string', // Primary contact facebook page
    ig_page: 'string', // Primary contact instagram page
    adt_cts: [adtContact]
}, {
    timestamps: true,
    versionKey: false
});

const organizer = model('Organizer', schema);

module.exports = organizer;