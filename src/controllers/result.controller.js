const model = require('../models/result.model');
const controller = {};

controller.getPartRes = async (req, res) => {
    const { part } = req.params;
    const results = await model.find({ part: part });
    res.json({
        status: 'success',
        body: results
    });
};

controller.getEvRes = async (req, res) => {
    const { ev } = req.params;
    const results = await model.find({ event: ev });
    res.json({
        status: 'success',
        body: results
    });
};

controller.getEvCatRes = async (req, res) => {
    const { ev, cat } = req.params;
    const results = await model.find({ event: ev, cat: cat });
    res.json({
        status: 'success',
        body: results
    });
};

controller.getEvCatPartRes = async (req, res) => {
    const { ev, cat, part } = req.params;
    const results = await model.find({ event: ev, cat: cat, part: part });
    res.json({
        status: 'success',
        body: results
    });
};

controller.setOne = async (req, res) => {
    const {
        event,
        cat,
        part,
        time,
        position
    } = req.body

    const newRes = new model({
        event,
        cat,
        part,
        time,
        position
    });

    await newRes.save();

    res.json({
        status: 'success',
        body: 'created'
    });
};

controller.update = async (req, res) => {
    const { id } = req.params;
    const {
        event,
        cat,
        part,
        time,
        position
    } = req.body

    const upd = await model.findByIdAndUpdate(id, {
        event,
        cat,
        part,
        time,
        position
    });

    res.json({
        status: 'success',
        body: upd
    });
};

controller.delete = async (req, res) => {
    const { id } = req.params;
    const deleted = await model.findByIdAndDelete(id);
    res.json({
        status: 'success',
        body: deleted
    });
};

module.exports = controller;