const { Schema } = require('mongoose');

const schema = new Schema({
    name: 'string',
    price: Number,
    availability: Number,
    first_number: Number,
    last_number: Number, //TODO: campo calculado
    minimum_age: Number
}, {
    timestamps: false,
    versionKey: false
});

module.exports = schema;