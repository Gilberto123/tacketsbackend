const { Schema } = require('mongoose');

const schema = new Schema({
    code: 'string',
    amt_discount: Number,
    desc: 'string', //TODO descripción
    perc_discount: 'string',
    limit_date: 'string',
    status: 'string' // TODO activo - inactivo
    //TODO ligado por evento
},{
    timestamps: true,
    versionKey: false
});

module.exports = schema;